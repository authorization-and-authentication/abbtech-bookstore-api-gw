package org.abbtech.practice.solid.interfaceseg;

public interface Worker {

    void work();

    void eat();
}
