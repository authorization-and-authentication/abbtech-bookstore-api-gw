package org.abbtech.practice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/operation")
@RequiredArgsConstructor
@Validated
public class OperationController {
    @GetMapping
    public void getOperation(){
        System.out.println("operation called");
    }
}
